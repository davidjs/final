import images

def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple, to_change_to: tuple) -> list[list[tuple[int, int, int]]]:
    imagen_ch = image

    for x in range(len(image)):
        for y in range(len(image[x])):
            if image[x][y] == to_change:
                imagen_ch[x][y] = to_change_to

    return imagen_ch

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    imagen_gir = []
    anchura, altura = images.size(image)
    for y in range(altura -1, -1, -1):
        columna_gir = []
        for x in range(0, anchura):
            pixel = image[x][y]
            columna_gir.append(pixel)
            if x == anchura -1:
                imagen_gir.append(columna_gir)
    return imagen_gir

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    image_mir = image
    image_mir.reverse()
    return image_mir

def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    imagen_rt = images.create_blank(width, height)

    for x in range(len(image)):
        for y in range(len(image[x])):
            pixel_col: list = []

            for color in image[x][y]:
                if color + increment not in range(0, 256):
                    if color + increment < 0:
                        color = 255 - (abs(color + increment) % 255)
                    else:
                        color = (abs(color + increment) % 255)
                else:
                    color =+ increment

                pixel_col.append(color)

                if len(pixel_col) == 3:
                   imagen_rt[x][y] = tuple(pixel_col)

    return imagen_rt
imagenrt=images.read_img("cafe.jpg")
cambio=rotate_colors(imagenrt,255)
images.write_img(cambio,"cafe_rc.jpg")

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
   width, height = images.size(image)
   imagen_blur = images.create_blank(width, height)

   for x in range(len(image)):
       for y in range(len(image[x])):

           if x == 0 and y == 0:
               #esquina superior izquierda
               r1, g1, b1 = image[x+1][y]
               r2, g2, b2 = image[x][y+1]
               imagen_blur[x][y] = ((r1+r2)//2, (g1+g2)//2, (b1+b2)//2)
           elif x == 0 and y == len(image[x])-1:
               #esquina inferior izquierda
               r1, g1, b1 = image[x + 1][y]
               r2, g2, b2 = image[x][y - 1]
               imagen_blur[x][y] = ((r1 + r2) // 2, (g1 + g2) // 2, (b1 + b2) // 2)
           elif x == len(image)-1 and y == 0:
               #esquina superior derecha
               r1, g1, b1 = image[x-1][y]
               r2, g2, b2 = image[x][y+1]
               imagen_blur[x][y] = ((r1 + r2) // 2, (g1 + g2) // 2, (b1 + b2) // 2)
           elif x == len(image)-1 and y == len(image[x])-1:
               #esquina inferior derecha
               r1, g1, b1 = image[x-1][y]
               r2, g2, b2 = image[x][y-1]
               imagen_blur[x][y] = ((r1 + r2) // 2, (g1 + g2) // 2, (b1 + b2) // 2)
           elif x in range(1, len(image)-1) and y in range(1, len(image[x])-1):
               #interior
               r1, g1, b1 = image[x-1][y]
               r2, g2, b2 = image[x+1][y]
               r3, g3, b3 = image[x][y+1]
               r4, g4, b4 = image[x][y-1]
               imagen_blur[x][y] = ((r1+r2+r3+r4) // 4, (g1+g2+g3+g4)//4, (b1+b2+b3+b4)//4)
           elif x == 0:
               #borde izquierdo
               r1, g1, b1 = image[x][y+1]
               r2, g2, b2 = image[x][y-1]
               r3, g3, b3 = image[x+1][y]
               imagen_blur[x][y] = ((r1+r2+r3)//3, (g1+g2+g3)//3, (b1+b2+b3)//3)
           elif x == len(image)-1:
               #borde derecho
               r1, g1, b1 = image[x][y-1]
               r2, g2, b2 = image[x][y+1]
               r3, g3, b3 = image[x-1][y]
               imagen_blur[x][y] = ((r1+r2+r3)//3, (g1+g2+g3)//3, (b1+b2+b3)//3)
           elif y == 0:
               #borde superior
               r1, g1, b1 = image[x+1][y]
               r2, g2, b2 = image[x-1][y]
               r3, g3, b3 = image[x][y+1]
               imagen_blur[x][y] = ((r1+r2+r3)//3, (g1+g2+g3)//3, (b1+b2+b3)//3)
           elif y == len(image[x])-1:
               #borde inferior
               r1, g1, b1 = image[x+1][y]
               r2, g2, b2 = image[x-1][y]
               r3, g3, b3 = image[x][y-1]
               imagen_blur[x][y] = ((r1+r2+r3)//3, (g1+g2+g3)//3, (b1+b2+b3)//3)

   return imagen_blur

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    image_sh:list = []

    if horizontal > 0:
        filas_sh = image[len(image)-horizontal : len(image)] + image[0:len(image)-horizontal]

    if horizontal < 0:
        filas_sh = image[-horizontal:len(image)] + image[0:-horizontal]

    if vertical > 0:
        for x in range(0, len(filas_sh)):
            columnas_sh = filas_sh[x][vertical:len(filas_sh)] + filas_sh[x][0:vertical]
            image_sh.append(columnas_sh)
    if vertical < 0:
        for x in range(0, len(filas_sh)):
            columnas_sh = filas_sh[x][len(filas_sh) - abs(vertical-1): len(filas_sh)] + filas_sh[x][0:len(filas_sh) - abs(vertical-1)]
            image_sh.append(columnas_sh)

    return image_sh
imagenrgb=images.read_img("cafe.jpg")
mover=shift(imagenrgb,150,160)
images.write_img(mover,"cafe_sh.jpg")

def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    image_sh = images.create_blank(width,height)
    rango = image[x: x + width]

    for i in range(0, len(rango)):
        del rango[i][: y]
        del rango[i][height : len(rango[i])]

    for i in range(0, len(rango)):
        for j in range(0, len(rango[i])):
            image_sh[i][j] = rango[i][j]

    return image_sh

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    imagen_gr = images.create_blank(width, height)

    for x in range(len(imagen_gr)):
        for y in range(len(imagen_gr[x])):
            r, g, b = image[x][y]
            gray = (r + g + b) // 3
            imagen_gr[x][y] = (gray, gray, gray)

    return imagen_gr

def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    imagen_filter = images.create_blank(width, height)

    for x in range(len(image)):
        for y in range(len(image[x])):
            r1, g1, b1 = image[x][y]

            r2 = int(r1 * r)
            g2 = int(g1 * g)
            b2 = int(b1 * b)
            if r2 > 255:
                r2 = 255
            if g2 > 255:
                g2 = 255
            if b2 > 255:
                b2 = 255
            imagen_filter[x][y] = (r2, g2, b2)

    return imagen_filter

